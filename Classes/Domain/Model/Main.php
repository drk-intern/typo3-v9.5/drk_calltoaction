<?php

namespace Drk\Calltoaction\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Main
 */
class Main extends AbstractEntity
{
    /**
     * calltoactiontype
     *
     * @var int
     */
    protected $calltoactiontype = 0;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * icon
     *
     * @var FileReference
     */
    protected $icon = null;

    /**
     * image
     *
     * @var FileReference
     */
    protected $image = null;

    /**
     * bodytext
     *
     * @var string
     */
    protected $bodytext = '';

    /**
     * link
     *
     * @var int
     */
    protected $link = 0;

    /**
     * linktext
     *
     * @var string
     */
    protected $linktext = '';

    /**
     * amount
     *
     * @var float
     */
    protected $amount = 0.0;

    /**
     * purpose
     *
     * @var int
     */
    protected $purpose = 0;

    /**
     * advertising_code
     *
     * @var int
     */
    protected $advertising_code = 0;

    /**
     * Returns the calltoactiontype
     *
     * @return int $calltoactiontype
     */
    public function getCalltoactiontype()
    {
        return $this->calltoactiontype;
    }

    /**
     * Sets the calltoactiontype
     *
     * @param int $calltoactiontype
     *
     * @return void
     */
    public function setCalltoactiontype($calltoactiontype)
    {
        $this->calltoactiontype = $calltoactiontype;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the icon
     *
     * @return FileReference $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets the icon
     *
     * @param FileReference $icon
     *
     * @return void
     */
    public function setIcon(FileReference $icon)
    {
        $this->icon = $icon;
    }

    /**
     * Returns the image
     *
     * @return FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param FileReference $image
     *
     * @return void
     */
    public function setImage(FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the bodytext
     *
     * @return string $bodytext
     */
    public function getBodytext()
    {
        return $this->bodytext;
    }

    /**
     * Sets the bodytext
     *
     * @param string $bodytext
     *
     * @return void
     */
    public function setBodytext($bodytext)
    {
        $this->bodytext = $bodytext;
    }

    /**
     * Returns the link
     *
     * @return int $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Sets the link
     *
     * @param int $link
     *
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Returns the linktext
     *
     * @return string $linktext
     */
    public function getLinktext()
    {
        return $this->linktext;
    }

    /**
     * Sets the linktext
     *
     * @param string $linktext
     *
     * @return void
     */
    public function setLinktext($linktext)
    {
        $this->linktext = $linktext;
    }

    /**
     * Returns the amount
     *
     * @return float $amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Sets the amount
     *
     * @param float $amount
     *
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Returns the purpose
     *
     * @return int $purpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Sets the purpose
     *
     * @param int $purpose
     *
     * @return void
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;
    }

    /**
     * @return int
     */
    public function getAdvertiseCode()
    {
        return $this->advertising_code;
    }

    /**
     * @param int $advertiseCode
     */
    public function setAdvertiseCode($advertiseCode)
    {
        $this->advertising_code = $advertiseCode;
    }
}
