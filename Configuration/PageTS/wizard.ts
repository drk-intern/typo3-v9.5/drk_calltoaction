// configure a bookmark element.
mod.wizards.newContentElement.wizardItems.special {
    elements.drk_calltoaction {
        iconIdentifier = icon-plugin-calltoaction
        title = LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.title
        description = LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.description
        tt_content_defValues {
            CType = drk_calltoaction
        }
    }

    show := addToList(drk_calltoaction)
}
