<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}
call_user_func(
    function () {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            'tt_content',
            'CType',
            array(
                'LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang_db.xlf:calltoaction.label',
                'drk_calltoaction'
            ),
            'textmedia',
            'after'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            'drk_calltoaction',
            'Configuration/PageTS/wizard.ts',
            'CallToAction PageTS'
        );
    }
);
