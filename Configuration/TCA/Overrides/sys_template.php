<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
call_user_func(
    function () {

        # =========================================================================== #
        # EXTENSION - KONFIGURATION
        # =========================================================================== #
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'drk_calltoaction',
            'Configuration/TypoScript',
            'CallToAction'
        );
    }
);
