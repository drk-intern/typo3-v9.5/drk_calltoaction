<?php
/**
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 */

call_user_func(
    function () {
        $languageFilePrefix = 'LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang_db.xlf:';
        $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

        // register this plugin
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'drk_calltoaction',
            'FrsDrkCalltoaction',
            'DRK Call to action',
            'icon-plugin-calltoaction'
        );

        // configure tt_content display in BE
        // Configure the default backend fields for the content element "Stage image"
        $GLOBALS['TCA']['tt_content']['types']['drk_calltoaction'] = array(
            'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
        tx_frsdrkcalltoaction_main;' . $frontendLanguageFilePrefix . 'header_link_formlabel,
        rowDescription'
        );

        // configure database field to inline relation
        $tempColumnsTtContent = array(
            'tx_frsdrkcalltoaction_main' => array(
                'exclude' => 1,
                'label' => $languageFilePrefix . 'calltoaction.label',
                'config' => array(
                    'type' => 'group',
                    'allowed' => 'tx_frsdrkcalltoaction_domain_model_main',
                    'size' => '1',
                    'maxitems' => '1',
                    'minitems' => '0',
                    'eval' => 'int',
                    'default' => 0
                ),
            ),
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
            'tt_content',
            $tempColumnsTtContent
        );

        $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['drk_calltoaction'] = 'icon-plugin-calltoaction';
    }
);
