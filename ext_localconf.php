<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'drk_calltoaction',
            'Main',
            array(
                \Drk\Calltoaction\Controller\MainController::class => 'show',

            ),
            // non-cacheable actions
            array(
                \Drk\Calltoaction\Controller\MainController::class => '',

            )
        );
        // wizards
        /*\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.forms {
                    elements.drk_calltoaction {
                        iconIdentifier = icon-plugin-calltoaction
                        title = LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.title
                        description = LLL:EXT:drk_calltoaction/Resources/Private/Language/locallang.xlf:wizard.description
                        tt_content_defValues {
                            CType = drk_calltoaction
                        }
                    }
                show := addToList(drk_calltoaction)
                }
           }'
        );*/
        // do not use "use" for \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider due to TYPO3 caching bug!
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon('icon-plugin-calltoaction', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
            'source' => 'EXT:drk_calltoaction/Resources/Public/Icons/DRK_Backend_Icons_Bookmark.svg'
        ));
    },
    'drk_calltoaction'
);
